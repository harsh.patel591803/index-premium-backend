// const cron = require('node-cron');
// const mongoose = require('mongoose');
// const express = require('express');
// const http = require('http');
// const socketIo = require('socket.io');
// const app = express();
// const server = http.createServer(app);
// const cors = require('cors');
// // const fetch = require('node-fetch');

// const symbols = ['NIFTY', 'BANKNIFTY', 'FINNIFTY', 'MIDCPNIFTY', 'NIFTYNXT50'];

// app.use(cors());
// mongoose.connect('mongodb+srv://harsh:1234@cluster0.085ou8p.mongodb.net/', { useNewUrlParser: true, useUnifiedTopology: true });

// const StraddlePriceSchema = new mongoose.Schema({
//   straddlePrice: Number,
//   time: Date,
//   strikePrice: Number
// });

// const StraddlePrice = mongoose.model('StraddlePrice', StraddlePriceSchema);

// const io = socketIo(server, {
//   cors: {
//     origin: "http://localhost:3001",
//     methods: ["GET", "POST"],
//     allowedHeaders: ["Content-Type"],
//     credentials: true
//   }
// });

// // Save data every minute
// cron.schedule('* * * * *', fetchAndSaveData); // Every minute

// // Emit live data every 10 seconds
// cron.schedule('*/10 * * * * *', () => {
//   fetchData2()
//   // .then(data => {
//   //   console.log("Data : ", data);
//   //   io.emit('liveData', data); // Emitting data to all connected clients
//   // });
// });

// function fetchAndSaveData() {
//   fetchData1()
//   // .then(data => {
//   //   console.log("fetchData : ", data);
//   //   const dataPoint = new StraddlePrice(data);
//   //   dataPoint.save().catch(err => console.error('Error saving data:', err));
//   // });
// }

// async function fetchData1() {
//   try {
//     const response = await fetch('https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY');
//     const data = await response.json();
//     return handleData1(data);
//   } catch (error) {
//     return console.error('Error fetching data:', error);
//   }
// }
// async function fetchData2() {
//   try {
//     const response = await fetch('https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY');
//     const data = await response.json();
//      handleData2(data);

//   } catch (error) {
//     return console.error('Error fetching data:', error);
//   }
// }

// function handleData1(data) {
//   // Assuming data is already parsed and formatted appropriately
//   try {
//     // Assuming 'data.records' contains the relevant data
//     const records = data.records.data;
//     const underlyingValue = data.records.underlyingValue;
//     const latestExpiry = data.records.expiryDates[0];

//     // Find the closest option based on strike price
//     let closest = records.reduce((prev, curr) => {
//       return (Math.abs(curr.strikePrice - underlyingValue) < Math.abs(prev.strikePrice - underlyingValue) ? curr : prev);
//     });

//     // Filter options for the latest expiry only and the closest strike price
//     const latestExpiryOptions = records.filter(option => option.expiryDate === latestExpiry && option === closest);

//     latestExpiryOptions.forEach(option => {
//       if (option.PE && option.CE) {
//         const straddleData = {
//           strikePrice: option.strikePrice,
//           straddlePrice: option.PE.lastPrice + option.CE.lastPrice,
//           time: new Date() // Using the current date and time
//         };

//         // Save the straddle data to MongoDB
//         const straddleInstance = new StraddlePrice(straddleData);
//         straddleInstance.save()
//           .then(doc => {
//             console.log('Data saved:', doc);
//             // Emit the data to all connected clients
//             // io.emit('data', straddleData);
//           })
//           .catch(err => {
//             console.error('Error saving data:', err);
//           });
//       }
//     });
//   } catch (error) {
//     console.error('Error handling data:', error);
//   }
// }

// function handleData2(data) {
//   // Assuming data is already parsed and formatted appropriately
//   try {
//     // Assuming 'data.records' contains the relevant data
//     const records = data.records.data;
//     const underlyingValue = data.records.underlyingValue;
//     const latestExpiry = data.records.expiryDates[0];

//     // Find the closest option based on strike price
//     let closest = records.reduce((prev, curr) => {
//       return (Math.abs(curr.strikePrice - underlyingValue) < Math.abs(prev.strikePrice - underlyingValue) ? curr : prev);
//     });

//     // Filter options for the latest expiry only and the closest strike price
//     const latestExpiryOptions = records.filter(option => option.expiryDate === latestExpiry && option === closest);

//     latestExpiryOptions.forEach(option => {
//       if (option.PE && option.CE) {
//         const straddleData = {
//           strikePrice: option.strikePrice,
//           straddlePrice: option.PE.lastPrice + option.CE.lastPrice,
//           time: new Date() // Using the current date and time
//         };

//         io.emit('data', straddleData);

//         // Save the straddle data to MongoDB
//         // const straddleInstance = new StraddlePrice(straddleData);
//         // straddleInstance.save()
//         //   .then(doc => {
//         //     console.log('Data saved:', doc);
//         //     // Emit the data to all connected clients
//         //     // io.emit('data', straddleData);
//         //   })
//         //   .catch(err => {
//         //     console.error('Error saving data:', err);
//         //   });
//       }
//     });
//   } catch (error) {
//     console.error('Error handling data:', error);
//   }
// }

// app.get('/api/straddlePrices', async (req, res) => {
//   const today = new Date();
//   today.setHours(0, 0, 0, 0);

//   try {
//     const prices = await StraddlePrice.find({
//       time: { $gte: today }
//     }).sort({ time: 1 });
//     res.json(prices);
//   } catch (error) {
//     res.status(500).send(error);
//   }
// });

// server.listen(8000, () => {
//   console.log('Server is running on port 6000');
// });




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// const express = require('express');
// const http = require('http');
// const socketIo = require('socket.io');
// const cors = require('cors');
// const app = express();
// const server = http.createServer(app);


// const port = 5000;
// const cron = require('node-cron');
// const mongoose = require('mongoose'); // You will need to set up mongoose if you haven't already




// app.use(cors());

// // Socket.io setup with CORS options
// const io = socketIo(server, {
//   cors: {
//     origin: "http://localhost:3000", // Frontend URL
//     methods: ["GET", "POST"],
//     allowedHeaders: ["Content-Type"],
//     credentials: true
//   }
// });

// // MongoDB connection setup
// mongoose.connect('mongodb+srv://harsh:1234@cluster0.085ou8p.mongodb.net/', { useNewUrlParser: true, useUnifiedTopology: true });

// // Define a schema for your data
// const DataSchema = new mongoose.Schema({
//   straddlePrice: Number,
//   time: String,
// });

// const db = mongoose.connection;
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// db.once('open', () => {
//     console.log('Connected to the database.');
// });

// // const mongoose = require('mongoose');

// // Define a schema for the straddle prices
// const StraddlePriceSchema = new mongoose.Schema({
//   straddlePrice: Number,
//   time: Date,
//   strikePrice: Number
// });

// // Create a model from the schema
// const StraddlePrice = mongoose.model('StraddlePrice', StraddlePriceSchema);

// // Schedule tasks to be run on the server.
// cron.schedule('37 18 * * *', () => {
//   console.log('Running a task every day at 9:15 AM');
//   const interval = setInterval(fetchAndEmitData, 10000); // Fetch data every 10 seconds

//   setTimeout(() => {
//     clearInterval(interval); // Stop the interval after the market closes or at a specific time
//   }, marketDuration); // Calculate market duration or set a fixed time
// });

// function fetchAndEmitData() {
//   fetch('https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY')
//     .then(response => response.json())
//     .then(data => handleData(data))
//     .catch(error => console.error('Error fetching data:', error));
// }
// function handleData(data) {
//   // Assuming data is already parsed and formatted appropriately
//   try {
//     // Assuming 'data.records' contains the relevant data
//     const records = data.records.data;
//     const underlyingValue = data.records.underlyingValue;
//     const latestExpiry = data.records.expiryDates[0];

//     // Find the closest option based on strike price
//     let closest = records.reduce((prev, curr) => {
//       return (Math.abs(curr.strikePrice - underlyingValue) < Math.abs(prev.strikePrice - underlyingValue) ? curr : prev);
//     });

//     // Filter options for the latest expiry only and the closest strike price
//     const latestExpiryOptions = records.filter(option => option.expiryDate === latestExpiry && option === closest);

//     latestExpiryOptions.forEach(option => {
//       if (option.PE && option.CE) {
//         const straddleData = {
//           strikePrice: option.strikePrice,
//           straddlePrice: option.PE.lastPrice + option.CE.lastPrice,
//           time: new Date() // Using the current date and time
//         };

//         // Save the straddle data to MongoDB
//         const straddleInstance = new StraddlePrice(straddleData);
//         straddleInstance.save()
//           .then(doc => {
//             console.log('Data saved:', doc);
//             // Emit the data to all connected clients
//             // io.emit('data', straddleData);
//           })
//           .catch(err => {
//             console.error('Error saving data:', err);
//           });
//       }
//     });
//   } catch (error) {
//     console.error('Error handling data:', error);
//   }
// }



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


const cron = require('node-cron');
const mongoose = require('mongoose');
const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const app = express();
const server = http.createServer(app);
const cors = require('cors');
// const fetch = require('node-fetch');

const symbols = ['NIFTY', 'BANKNIFTY', 'FINNIFTY'];

app.use(cors());
mongoose.connect('mongodb+srv://harsh:1234@cluster0.085ou8p.mongodb.net/', { useNewUrlParser: true, useUnifiedTopology: true });

const StraddlePriceSchema = new mongoose.Schema({
  straddlePrice: Number,
  time: Date,
  strikePrice: Number
});

const StraddlePrice = mongoose.model('StraddlePrice', StraddlePriceSchema);

const io = socketIo(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    allowedHeaders: ["Content-Type"],
    credentials: true
  }
});



// Emit live data every 10 seconds
// cron.schedule('*/10 * * * * *', () => {
//   fetchData2()
//   // .then(data => {
//   //   console.log("Data : ", data);
//   //   io.emit('liveData', data); // Emitting data to all connected clients
//   // });
// });

cron.schedule('*/10 * * * * *', fetchDataForSymbols);



async function fetchDataForSymbols() {
  symbols.forEach(async symbol => {
    try {
      const response = await fetch(`https://www.nseindia.com/api/option-chain-indices?symbol=${symbol}`);
      const data = await response.json();

      // console.log('DATA : ', data);

      handleData2(data, symbol); // Assume processData formats the data correctly
      // io.emit(`data-${symbol}`, straddleData); // Emitting data for each symbol
    } catch (error) {
      console.error(`Error fetching data for ${symbol}:`, error);
    }
  });
}
// async function fetchData2() {
//   try {
//     const response = await fetch('https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY');
//     const data = await response.json();
//      handleData2(data);

//   } catch (error) {
//     return console.error('Error fetching data:', error);
//   }
// }



function handleData2(data, symbol) {
  // Assuming data is already parsed and formatted appropriately
  try {
    // Assuming 'data.records' contains the relevant data
    const records = data.records.data;
    const underlyingValue = data.records.underlyingValue;
    const latestExpiry = data.records.expiryDates[0];

    // Find the closest option based on strike price
    let closest = records.reduce((prev, curr) => {
      return (Math.abs(curr.strikePrice - underlyingValue) < Math.abs(prev.strikePrice - underlyingValue) ? curr : prev);
    });

    // Filter options for the latest expiry only and the closest strike price
    const latestExpiryOptions = records.filter(option => option.expiryDate === latestExpiry && option === closest);

    latestExpiryOptions.forEach(option => {
      if (option.PE && option.CE) {
        const straddleData = {
          symbol: symbol,
          strikePrice: option.strikePrice,
          straddlePrice: option.PE.lastPrice + option.CE.lastPrice,
          time: new Date() // Using the current date and time
        };

        console.log('STRADDLE DATA : ', straddleData);

        io.emit(`data-${symbol}`, straddleData);

        // Save the straddle data to MongoDB
        // const straddleInstance = new StraddlePrice(straddleData);
        // straddleInstance.save()
        //   .then(doc => {
        //     console.log('Data saved:', doc);
        //     // Emit the data to all connected clients
        //     // io.emit('data', straddleData);
        //   })
        //   .catch(err => {
        //     console.error('Error saving data:', err);
        //   });
      }
    });
  } catch (error) {
    console.error('Error handling data:', error);
  }
}


// function handleData2(data) {
//   // Assuming data is already parsed and formatted appropriately
//   try {
//     // Assuming 'data.records' contains the relevant data
//     const records = data.records.data;
//     const underlyingValue = data.records.underlyingValue;
//     const latestExpiry = data.records.expiryDates[0];

//     // Find the closest option based on strike price
//     let closest = records.reduce((prev, curr) => {
//       return (Math.abs(curr.strikePrice - underlyingValue) < Math.abs(prev.strikePrice - underlyingValue) ? curr : prev);
//     });

//     // Filter options for the latest expiry only and the closest strike price
//     const latestExpiryOptions = records.filter(option => option.expiryDate === latestExpiry && option === closest);

//     latestExpiryOptions.forEach(option => {
//       if (option.PE && option.CE) {
//         const straddleData = {
//           strikePrice: option.strikePrice,
//           straddlePrice: option.PE.lastPrice + option.CE.lastPrice,
//           time: new Date() // Using the current date and time
//         };

//         io.emit('data', straddleData);

//         // Save the straddle data to MongoDB
//         // const straddleInstance = new StraddlePrice(straddleData);
//         // straddleInstance.save()
//         //   .then(doc => {
//         //     console.log('Data saved:', doc);
//         //     // Emit the data to all connected clients
//         //     // io.emit('data', straddleData);
//         //   })
//         //   .catch(err => {
//         //     console.error('Error saving data:', err);
//         //   });
//       }
//     });
//   } catch (error) {
//     console.error('Error handling data:', error);
//   }
// }

app.get('/api/straddlePrices', async (req, res) => {
  const today = new Date();
  today.setHours(0, 0, 0, 0);

  try {
    const prices = await StraddlePrice.find({
      time: { $gte: today }
    }).sort({ time: 1 });
    res.json(prices);
  } catch (error) {
    res.status(500).send(error);
  }
});

server.listen(8005, () => {
  console.log('Server is running on port 8005');
});

